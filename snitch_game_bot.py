#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pickle
import random
import math
from enum import Enum

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram import ParseMode, ReplyKeyboardMarkup

# Enable logging
import logging

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def log(stuff):
    print(remove_non_ascii(str(stuff)))


def remove_non_ascii(string):
    result = []
    for c in string:
        if 31 < ord(c) < 127 or c == '\n' or c == '\t':
            result.append(c)
    return ''.join(result)


emojis = {
    "0": "🅾️️",
    "1": "1️⃣",
    "2": "2️⃣",
    "3": "3️⃣",
    "4": "4️⃣",
    "5": "5️⃣",
    "6": "6️⃣",
    "7": "7️⃣",
    "8": "8️⃣",
    "9": "9️⃣",
    "X": "❌",
    "SNITCH": "🕶",
    "MUSCLE": "💪",
    "LOOKOUT": "🔦",
    "SCAMMER": "💰",
    "LOCKPICK": "🔓",
    "DRIVER": "🚐",
}

players = {}


class Player:
    def __init__(self, user):
        self.user = user
        self.coins = 3
        self.hand = [None] * 10
        self.played = [True] * 10
        self.selected_to_play = -1

    def __repr__(self):
        return f"@{self.username} ${self.coins}"

    @property
    def id(self):
        return self.user.id

    @property
    def username(self):
        if self.user is not None:
            return self.user.username
        return self.user.first_name

    @property
    def snitches_left(self):
        return 3 - sum(self.played[7:])

    @property
    def final_score(self):
        return self.coins + self.snitches_left / 4

    def print_hand(self):
        global emojis
        text = "Your hand:\n\n"
        available_cards = [self.hand[x] for x in range(10) if self.played[x] is False]
        for role in Game.characters:
            text += f"{emojis[str(available_cards.count(role))]} ✖ {Game.card_text(role)}\n"
        return text


class State(Enum):
    ACCEPTING_PLAYERS = 1
    HEIST_SELECTION = 2
    CARD_PLAYING = 3
    GAME_FINISHED = 4


class Card(Enum):
    SNITCH = 0
    MUSCLE = 1
    LOOKOUT = 2
    SCAMMER = 3
    LOCKPICK = 4
    DRIVER = 5


games = {}


class Game:
    state_text = {
        State.ACCEPTING_PLAYERS: "Accepting players",
        State.HEIST_SELECTION: "Heist selection",
        State.CARD_PLAYING: "Card playing",
        State.GAME_FINISHED: "Game finished"
    }
    characters = [
        Card.MUSCLE,
        Card.LOOKOUT,
        Card.SCAMMER,
        Card.LOCKPICK,
        Card.DRIVER,
        Card.SNITCH
    ]

    def __init__(self, group_id=0, debug_mode=False):
        self.group_id = group_id
        self.state = State.ACCEPTING_PLAYERS
        self.players = []
        self.cards = Game.characters[:5] * 13
        self.round = -1
        self.turn = -1
        self.heist = []
        self.debug_mode = debug_mode

    def __repr__(self):
        return f"[{self.group_id}] {self.status} {self.players}"

    @property
    def status(self):
        return Game.state_text[self.state]

    @property
    def card_names(self):
        return []

    def print_game(self):
        return '\n'.join([f"Game status: {self.status}",
                          f"Players joined:",
                          f"{self.print_balances()}"])

    def print_balances(self):
        return '\n'.join([str(x) for x in self.players])

    @staticmethod
    def card_text(card):
        return f"{emojis[card.name]}️ {card.name[0]}{card.name[1:].lower()}"

    @staticmethod
    def card_name(emoji):
        global emojis
        for card in Game.characters:
            if emojis[card.name] == emoji:
                return card.name
        return None

    def distribute_cards(self):
        random.shuffle(self.cards)
        for i in range(len(self.players)):
            self.players[i].hand = sorted(self.cards[0:7], key=lambda x: x.value) + [Card.SNITCH] * 3
            self.players[i].played = [False] * 10
            self.cards = self.cards[7:]

    def resolve_round(self):
        successful_heist = True
        table = [x.hand[x.selected_to_play] for x in self.players]
        for role in Game.characters[:5]:
            if table.count(role) < self.heist.count(role):
                successful_heist = False
                break
        snitches = table.count(Card.SNITCH)
        non_snitches = len(self.players) - snitches

        if successful_heist:
            for player in self.players:
                if player.hand[player.selected_to_play] is not Card.SNITCH:
                    player.coins += len(self.heist)

            report1 = f"Contributors each get {len(self.heist)} coins:\n"
            report2 = "" if snitches == 0 else "Snitches each get 0 coins:\n"
            for player in self.players:
                if player.hand[player.selected_to_play] is not Card.SNITCH:
                    report1 += "@{}\n".format(player.username)
                else:
                    report2 += "@{}\n".format(player.username)

        else:
            snitches_loot = 0 if snitches == 0 else math.ceil(non_snitches / snitches)
            for player in self.players:
                if player.hand[player.selected_to_play] is Card.SNITCH:
                    player.coins += snitches_loot
                else:
                    player.coins -= 1

            if non_snitches > 0:
                report1 = "Non-snitches each lose 1 coin:\n"
                report2 = f"Snitches share the loot and each get {snitches_loot} coins:\n"
                for player in self.players:
                    if player.hand[player.selected_to_play] is Card.SNITCH:
                        report2 += "@{} \n".format(player.username)
                    else:
                        report1 += "@{} \n".format(player.username)
                if snitches == 0:
                    report1 = "despite any snitches. Everyone lose 1 coin.\n"
                    report2 = ""
            else:
                report1 = "Everybody snitched! No one gets anything!"
                report2 = ""

        return successful_heist, report1 + report2

    def begin_next_round(self, context):
        global emojis
        for player in self.players:
            if -1 != player.selected_to_play:
                player.played[player.selected_to_play] = True
            player.selected_to_play = -1
            custom_keyboard = [[emojis[Card.MUSCLE.name], emojis[Card.LOOKOUT.name], emojis[Card.SCAMMER.name]],
                               [emojis[Card.LOCKPICK.name], emojis[Card.DRIVER.name], emojis[Card.SNITCH.name]]]
            reply_markup = ReplyKeyboardMarkup(custom_keyboard)
            context.bot.send_message(chat_id=player.id, text=player.print_hand(), reply_markup=reply_markup)

        self.round += 1
        if self.round >= 8:
            self.state = State.GAME_FINISHED
            self.finish(context)
            return

        context.bot.send_message(chat_id=self.group_id,
                                 text=f"Round {self.round + 1}\nPlayers' loots:\n{self.print_balances()}")

        self.state = State.HEIST_SELECTION
        self.turn = (int(random.random() * len(self.players)) if self.turn == -1 else self.turn + 1) % len(self.players)

        text = '\n'.join([f"It's @{self.players[self.turn].username} 's turn.",
                          f"How big a heist you want to do?"] +
                         [f"/heist_{x}" for x in range(2 - self.debug_mode, len(self.players) + 1)]
                         )
        context.bot.send_message(chat_id=self.group_id, text=text)

    def finish(self, context):
        max_score = max([x.final_score for x in self.players])
        winners = [x for x in self.players if x.final_score == max_score]
        text = ("Game finished.\n" +
                "Final standings:\n" +
                "{}\n" +
                "The winner:\n" +
                "{} with ${} and {} snitches left.\n").format(
            self.print_balances(),
            ' and '.join(["@{}".format(x.username) for x in winners]),
            winners[0].coins,
            winners[0].snitches_left
        )
        context.bot.send_message(chat_id=self.group_id, text=text)

        for player_id in [x.id for x in self.players]:
            del players[player_id]

        del games[self.group_id]


def is_not_a_group(message):
    if message.chat.type != "group":
        message.reply_text("You can only use this command in a group.")
        return True
    return False


def is_not_private(message):
    if message.chat.type != "private":
        message.reply_text("You can only use this command in private chat.")
        return True
    return False


def game_not_in_progress(message):
    global games
    if message.chat.id not in games:
        message.reply_text("No games is going on!\n" +
                           "Create a new game with /newgame command.")
        return True
    return False


def already_joined(message):
    global players
    if message.from_user.id in players:
        message.reply_text("You have already joined a game.")
        return True
    return False


def not_a_player(message):
    global games, players
    if message.chat.type == "group" \
            and players.get(message.from_user.id, None) != message.chat.id:
        message.reply_text("You have not joined this game.")
        return True
    if message.chat.type == "private" and message.from_user.id not in players:
        message.reply_text("You have not joined any games yet.")
        return True
    return False


def not_their_turn_to_heist(message):
    global games
    game = games[message.chat.id]
    if game.state is not State.HEIST_SELECTION:
        message.reply_text("It's not heisting time!")
        return True
    if message.from_user.id != game.players[game.turn].id:
        message.reply_text("It's not your turn to heist.")
        return True
    return False


def not_card_playing_time(message):
    global games, players
    game = games[players[message.from_user.id]]
    if game.state is not State.CARD_PLAYING:
        message.reply_text("It's not card playing time!")
        return True
    return False


# ===============================================================================
#   Command Handlers
#

def endgame(update, context):
    if is_not_a_group(update.message):
        return

    global games, players
    if update.message.chat.id not in games:
        update.message.reply_text("No games to end!")
        return

    for player_id in [x.id for x in games[update.message.chat.id].players]:
        del players[player_id]

    del games[update.message.chat.id]
    update.message.reply_text("The game ended. Thanks! 🌟")
    context.bot.send_message(chat_id=update.message.chat.id, message="To create a new game use /newgame")


def newgame(update, context):
    if is_not_a_group(update.message):
        return

    global games
    if update.message.chat.id in games:
        update.message.reply_text("There is already a game going on!")
        return

    debug_mode = context.args is not None and len(context.args) == 1 and context.args[0] == "debug"

    games[update.message.chat.id] = Game(update.message.chat.id, debug_mode=debug_mode)
    update.message.reply_text("The game has just started. Enjoy!\nPlease join the game with /join command.")


def start(update, context):
    global games, players
    if update.message.chat.type == "private":
        update.message.reply_text("Hi! 👋😁")
        if update.message.from_user.id not in players:
            return
        if games[players[update.message.from_user.id]].state is State.ACCEPTING_PLAYERS:
            return
        hand(update, context)
        return

    if game_not_in_progress(update.message):
        return

    game = games[update.message.chat.id]
    if game.state is not State.ACCEPTING_PLAYERS:
        update.message.reply_text("❗️ The gama has already started.")
        return

    if len(game.players) < 3 and not game.debug_mode:
        update.message.reply_text("Sorry! We still need more players to start the game.")
        return

    game.distribute_cards()
    update.message.reply_text("The game has just started!")
    game.begin_next_round(context)


def join(update, context):
    if is_not_a_group(update.message) or already_joined(update.message) or game_not_in_progress(update.message):
        return

    global games, players
    game = games[update.message.chat.id]

    if game.state is not State.ACCEPTING_PLAYERS:
        update.reply_text("Sorry! The game in not accepting players anymore.\nSee you in the next game.")
        return

    if len(game.players) == 5:
        update.reply_text("Sorry! There is already 5 players in the game.\nSee you in the next game.")
        return

    player = Player(user=update.message.from_user)
    players[player.id] = game.group_id
    game.players.append(player)
    context.bot.send_message(chat_id=game.group_id, text=game.print_game())
    if len(game.players) == 5:
        context.bot.send_message(chat_id=game.group_id,
                                 text="The game is now full. Please use /start to start the game.")
        return
    context.bot.send_message(chat_id=player.id,
                             text="Thanks for joining the game.\nPlease wait for the game to be started.")


def status(update, context):
    if is_not_a_group(update.message) or game_not_in_progress(update.message):
        return

    global games
    game = games[update.message.chat.id]
    update.message.reply_text(game.print_game())


def heist_(update, context):
    heist_number = update.message.text.replace("/heist_", "")
    heist(update, context, heist_number=heist_number)


def heist(update, context, heist_number="0"):
    if heist_number == "0" and context.args is not None and len(context.args) == 1:
        heist_number = context.args[0]

    if is_not_a_group(update.message) or game_not_in_progress(update.message) or not_a_player(
            update.message) or not_their_turn_to_heist(update.message):
        return

    global games
    game = games[update.message.chat.id]

    try:
        heist_number = int(heist_number)
        if (heist_number < 2 or len(game.players) < heist_number) and not game.debug_mode:
            raise IndexError
    except (ValueError, IndexError) as e:
        log(e)
        update.message.reply_text("Invalid heist parameter.\nPlease try again.")
        return

    game.heist = game.cards[:heist_number]
    game.cards = game.cards[heist_number:]
    game.state = State.CARD_PLAYING

    heist_text = '\n'.join([Game.card_text(x) for x in game.heist])

    update.message.reply_text('\n'.join([
        "The heist is:",
        heist_text,
        "",
        "Now you can discuss with other players about what to play.",
        "Please send your selected card in private message @snitch_game_bot."])
    )
    for player in game.players:
        context.bot.send_message(chat_id=player.id, text=f"The heist:\n\n{heist_text}")


def play_(update, context):
    play(update, context, card=Game.card_name(update.message.text))


def play(update, context, card=None):
    arg = None
    if card is not None:
        arg = card
    elif context.args is not None and len(context.args) == 1:
        arg = context.args[0]

    if is_not_private(update.message) or not_a_player(update.message) or not_card_playing_time(update.message):
        return

    global games
    game = games[players[update.message.from_user.id]]
    player = [x for x in game.players if x.id == update.message.from_user.id][0]

    card = arg.lower()
    if card not in [x.name.lower() for x in Game.characters]:
        update.message.reply_text("Invalid play parameter.\nPlease try again.")
        return

    for i in range(10):
        if not player.played[i] and player.hand[i].name.lower().strip() == card:
            player.selected_to_play = i
            break
    if player.selected_to_play == -1:
        update.message.reply_text("You have no (more) {}s.\nPlease select another card.".format(card))
        return

    not_played_yet = [x for x in game.players if x.selected_to_play == -1]
    if len(not_played_yet) == 0:
        successful_heist, report = game.resolve_round()
        text = "Cards played:\n"
        for player in game.players:
            text += "@{} played {}\n".format(player.username, Game.card_text(player.hand[player.selected_to_play]))
        text += "The Heist " + ("succeeded" if successful_heist else "failed") + ".\n"
        text += report
        context.bot.send_message(chat_id=game.group_id, text=text)
        game.begin_next_round(context)
    else:
        context.bot.send_message(chat_id=game.group_id, text="Still waiting for {} to play.".format(
            '\n'.join(["@" + x.username for x in not_played_yet])))


def hand(update, context):
    if is_not_private(update.message) or not_a_player(update.message):
        return

    global games
    game = games[players[update.message.from_user.id]]
    player = [x for x in game.players if x.id == update.message.from_user.id][0]
    context.bot.send_message(chat_id=player.id, text=player.print_hand())


def debug(update, context):
    global games, players
    log("==== [ DEBUG ] =================================================================")
    log("Games:")
    log(games)
    log("-" * 80)
    log("Players:")
    log(players)
    log("=" * 80)


def help(update, context):
    update.message.reply_text('\n'.join([
        "* Public commands: *",
        "/newgame        _creates a new game_",
        "/endgame        _ends the game_",
        "/status              _shows the game status_", 
        "/join                   _used for joining the game_",
        "/start                 _starts the game_",
        "/heist\\_<n>        _used for making a heist_",
        "    e.g. /heist\\_2",
        "",
        "* Private commands: *",
        "/hand                 _shows your hand_",
        "/play\\_<card>    _used to play a card_",
        "    e.g. /play\\_snitch"])
        , parse_mode=ParseMode.MARKDOWN
    )


def error(update, context):
    logger.warning('Error: "%s"', context.error)
    if str(context.error) == "Forbidden: bot can't initiate conversation with a user":
        update.message.reply_text(
            f"Cannot initiate conversation with @{update.message.from_user.username}\n" +
            "Please send me a private message so I could send you messages."
        )
    # log(str(update).replace("'", '"').replace("True", "true").replace("False", "false"))


def main():
    try:
        with open('token.txt') as T:
            token = T.read().strip()
    except IOError:
        import sys
        sys.stderr.write("Error: Cannot open 'token.txt' file.\n")
        exit(1)

    global games, players
    try:
        with open('data.pickle', 'rb') as handle:
            games, players = pickle.load(handle)
    except IOError:
        pass

    updater = Updater(token, use_context=True)

    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("newgame", newgame))
    dp.add_handler(CommandHandler("endgame", endgame))
    dp.add_handler(CommandHandler("join", join))
    dp.add_handler(CommandHandler("status", status))
    dp.add_handler(CommandHandler("heist", heist))
    dp.add_handler(MessageHandler(Filters.regex("^(/heist_[0-9])$"), heist_))
    dp.add_handler(CommandHandler("play", play))
    dp.add_handler(MessageHandler(Filters.regex("^(🕶|💪|🔦|💰|🔓|🚐)$"), play_))
    dp.add_handler(CommandHandler("hand", hand))

    dp.add_handler(CommandHandler("debug", debug))

    dp.add_error_handler(error)

    updater.start_polling()

    updater.idle()

    with open('data.pickle', 'wb') as handle:
        pickle.dump((games, players), handle, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == '__main__':
    main()
